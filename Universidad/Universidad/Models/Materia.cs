﻿using System;
using System.Collections.Generic;

namespace Universidad.Models
{
    public partial class Materia
    {
        public Materia()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idmateria { get; set; }
        public string Nombre { get; set; }
        public int? Estado { get; set; }

        public ICollection<Matricula> Matricula { get; set; }
    }
}
